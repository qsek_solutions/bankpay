const path = require('path');

module.exports = function override(config) {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.alias,
      assets: path.resolve(__dirname, 'src/assets'),
      '@ui-kit': path.resolve(__dirname, 'src/ui-kit'),
    },
  };

  return config;
};
