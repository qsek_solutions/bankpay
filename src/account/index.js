import React from 'react';
// import $ from "jquery";
import { NavLink } from 'react-router-dom';

import './styles.scss';

import { Page, SideBar } from '@ui-kit';
import { Nav, Card, Table } from 'react-bootstrap';
// import { Container, Navbar,Nav, Form, Button  } from 'react-bootstrap';


export default class Account extends React.Component {

    componentDidMount() {
    }

    render() {
        return (

            <Page className="BP_Account BP_SideBar-page">
                <SideBar />
                <div className="BP_SideBar-page__content">
                    <div className="BP_SideBar-page__header">
                        <a href="#/" className="sideber-toggler d-inline-flex d-lg-none">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                <path d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </a>
                        <Nav>
                            <Nav.Item>
                                <NavLink to="/account" className="nav-link" activeClassName="active">Account</NavLink>
                            </Nav.Item>
                            <Nav.Item>
                                <NavLink to="login" className="nav-link" activeClassName="active">Log Out</NavLink>
                            </Nav.Item>
                        </Nav>
                    </div>

                    <div className="BP_Account__content-wrapper">
                        <h2 className="BP_page-title">Account</h2>
                        <Card className="BP_Account-card">
                            <Card.Header>
                                <Card.Title>User profile</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>First name</th>
                                            <td>John</td>
                                        </tr>
                                        <tr>
                                            <th>Last name</th>
                                            <td>Doe</td>
                                        </tr>
                                        <tr>
                                            <th>Role</th>
                                            <td>CEO</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                            <Card.Footer>
                                <p>Need to edit these details? <a href="#/">Start Chat</a></p>
                            </Card.Footer>
                        </Card>
                        <Card className="BP_Account-card">
                            <Card.Header>
                                <Card.Title>Account details</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Company Legal Name</th>
                                            <td>Company Name Ltd</td>
                                        </tr>
                                        <tr>
                                            <th>Company Number</th>
                                            <td>123456789</td>
                                        </tr>
                                        <tr>
                                            <th>Business Address</th>
                                            <td>123 High Street <br/>
                                                London <br/>
                                                NW1 8AN
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Store URL</th>
                                            <td>website.com</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                            <Card.Footer>
                                <p>Need to edit these details? <a href="#/">Start Chat</a></p>
                            </Card.Footer>
                        </Card>
                        <Card className="BP_Account-card">
                            <Card.Header>
                                <Card.Title>Payout information</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Account Name</th>
                                            <td>Company Name Ltd</td>
                                        </tr>
                                        <tr>
                                            <th>UK Bank Name</th>
                                            <td>HSBC Bank plc</td>
                                        </tr>
                                        <tr>
                                            <th>UK Sort code</th>
                                            <td>123456 </td>
                                        </tr>
                                        <tr>
                                            <th>UK account number</th>
                                            <td>987654321</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                            <Card.Footer>
                                <p>Need to edit these details? <a href="#/">Start Chat</a></p>
                            </Card.Footer>
                        </Card>
                        <Card className="BP_Account-card">
                            <Card.Header>
                                <Card.Title>Public information</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Trading name</th>
                                            <td>Trading Name</td>
                                        </tr>
                                        <tr>
                                            <th>Support Email</th>
                                            <td>support@yoursite.com</td>
                                        </tr>
                                        <tr>
                                            <th>Support Phone Number</th>
                                            <td>+441 &nbsp; &nbsp;  234 5678 901 </td>
                                        </tr>
                                        <tr>
                                            <th>Payment Ref</th>
                                            <td>Knowledge portal? Live chat?</td>
                                        </tr>
                                        <tr>
                                            <th>Support website</th>
                                            <td>website.com</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                            <Card.Footer>
                                <p>Need to edit these details? <a href="#/">Start Chat</a></p>
                            </Card.Footer>
                        </Card>
                    </div>
                </div>
            </Page>
        );
    }
}
