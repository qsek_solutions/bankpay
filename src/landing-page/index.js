import React from 'react';
import $ from "jquery";

import './styles.scss';

import { Page, NavBar, Footer } from '@ui-kit';
import { Col, Container, Row, InputGroup, FormControl, Button } from 'react-bootstrap';

import Logo from 'assets/images/bankpay-logo.png';
import Landing_BG from 'assets/images/hero-img.png';
import bankpayImg from 'assets/images/bankpay-card.png';
import rbs_logo from 'assets/images/rbs_logo.png';
import barclays_logo from 'assets/images/barclays_logo.png';
import hsbc_logo from 'assets/images/hsbc_logo.png';
import NatWest_logo from 'assets/images/NatWest_logo.png';
import standard_chartered_logo from 'assets/images/standard_chartered_logo.png';
import App_short from 'assets/images/App_short.png';
import CustomersBG from 'assets/images/loyalty-bankpay.png';
import squarespace from 'assets/images/squarespace.png';
import woo_commerce from 'assets/images/woo-commerce.png';
import magento from 'assets/images/magento.png';
import big_commerce from 'assets/images/big-commerce.png';
import instant_credits from 'assets/images/instant-credits.png';
import chargebacks from 'assets/images/chargebacks.png';
import bank_level_encryption from 'assets/images/bank-level-encryption.png';


export default class LandingPage extends React.Component {


    componentDidMount() {
     
        $(window).on("scroll touchmove", function() 
        {
            $(".BP_scrolling-slider-section").removeClass (function (index, className) {
                return (className.match (/(^|\s)--section-\S+/g) || []).join(' ');
            });
            if ($(document).scrollTop() - 500 < $(".BP-scroll_sec1").offset().top && $(document).scrollTop() < $(".BP-scroll_sec2").offset().top - 190) 
            {
                $('.BP_scrolling-slider-section').addClass("--section-1");
            } else if ($(document).scrollTop() > $(".BP-scroll_sec2").offset().top - 500 && $(document).scrollTop() < $(".BP-scroll_sec3").offset().top - 190) {
                $('.BP_scrolling-slider-section').addClass("--section-2");
            } else if ($(document).scrollTop() > $(".BP-scroll_sec3").offset().top - 500 && $(document).scrollTop() < $(".BP-scroll_sec4").offset().top - 190) {
                $('.BP_scrolling-slider-section').addClass("--section-3");
            } else if ($(document).scrollTop() > $(".BP-scroll_sec4").offset().top - 190) {
                $('.BP_scrolling-slider-section').addClass("--section-4");
            };

        });

    }

    render() {
        return (

            <Page className="BP_landingPage">
               <NavBar />
               <main className="BP_content-wrapper">
                    <section className="BP_landing-section">
                        <Container fluid>
                            <Row className="align-items-end">
                                <Col md={5} className="ml-auto">
                                    <div className="BP_landing-caseStudy">
                                        <p>Learn how an online clothing retailer unlocked an increase in conversions at checkout by over 175%.<a href="/#">See case study</a></p>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                        <div className="BP_landing-content">
                            <h1>We are building the future of payments.</h1>
                            <p>Enable a faster, cheaper and safer alternative to cards on your store. Start accepting Bankpay in the UK today.</p>
                            <InputGroup>
                                <FormControl placeholder="Your work email" aria-label="Your work email" aria-describedby="basic-addon2" />
                                <InputGroup.Append>
                                    <Button variant="outline-secondary">Contact Sales</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                        <div className="BP_landing-bg">
                            <img src={Landing_BG} className="img-fluid" alt="Landing Background" />
                        </div>
                        <div className="BP_landing-nav">
                           <Container>
                               <div className="BP_landing-nav__list">
                                    <a href="/#">instant credit</a>
                                    <a href="/#">1-click checkout</a>
                                    <a href="/#">zero disputes</a>
                               </div>
                           </Container>
                        </div>
                    </section>
                    <section className="BP_workWith-section">
                        <Container>
                            <Row>
                                <Col lg="8" className="m-auto">
                                    <div className="card BP_workWith-card">
                                        <Row>
                                            <Col md="7">
                                                <div className="BP_worWith-card__content">
                                                    <h4 className="BP_title">Bankpay works with your ecommerce platform</h4>
                                                    <p>Get in touch, our experts will help you go live and start transacting instantly.</p>
                                                    <a href="/#" className="btn btn-black">Get Started
                                                        <svg width="12" height="8" viewBox="0 0 12 8" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M11.8625 3.54332C11.8624 3.54313 11.8623 3.5429 11.8621 3.54271L9.41278 0.187869C9.22929 -0.0634537 8.9325 -0.0625185 8.74985 0.190062C8.56722 0.442611 8.56793 0.851095 8.75142 1.10245L10.3959 3.35484H0.46875C0.209859 3.35484 0 3.64368 0 4C0 4.35632 0.209859 4.64516 0.46875 4.64516H10.3959L8.75144 6.89755C8.56795 7.1489 8.56725 7.55739 8.74987 7.80994C8.93252 8.06255 9.22933 8.06342 9.4128 7.81213L11.8621 4.45729C11.8623 4.4571 11.8624 4.45687 11.8625 4.45668C12.0461 4.20448 12.0455 3.79468 11.8625 3.54332Z"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </Col>
                                            <Col md="5">
                                                <div className="BP_worWith-card__image">
                                                    <img src={squarespace} alt="Squarespace" className="img-fluid" />
                                                    <img src={woo_commerce} alt="Woo Commerce" className="img-fluid" />
                                                    <img src={big_commerce} alt="Big Commerce" className="img-fluid" />
                                                    <img src={magento} alt="Magento" className="img-fluid" />
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </Col>
                            </Row>
                            <div className="BP_superior-payment">
                                <Row className="align-items-center">
                                    <Col md={6}>
                                        <div className="BP_superior-payment__text">
                                            <h1 className="BP_title">A superior payments alternative</h1>
                                            <p className="BP_description">Bankpay uses Open Banking to bring you closer to your customers to enable faster, cheaper and safer payments and payouts. </p>
                                        </div>
                                    </Col>
                                    <Col md={4} className="ml-auto">
                                        <div className="bankpay-image">
                                            <img src={bankpayImg} alt="BankPay" className="img-fluid" />
                                        </div>
                                    </Col>
                                </Row>
                                <div className="BP_customers">
                                    <div className="BP_customers__item">
                                        <img src={rbs_logo} alt="Customers" className="img-fluid" />
                                    </div>
                                    <div className="BP_customers__item">
                                        <img src={barclays_logo} alt="Customers" className="img-fluid" />
                                    </div>
                                    <div className="BP_customers__item">
                                        <img src={hsbc_logo} alt="Customers" className="img-fluid" />
                                    </div>
                                    <div className="BP_customers__item">
                                        <img src={NatWest_logo} alt="Customers" className="img-fluid" />
                                    </div>
                                    <div className="BP_customers__item">
                                        <img src={standard_chartered_logo} alt="Customers" className="img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </Container>
                    </section>
                    <section className="BP_scrolling">
                        <div className="BP_scrolling-slider-section --section-1 d-none d-md-block">
                            <Container className="">
                                <div className="BP_product-detaile__img">
                                    <div className="BP_product-detaile__content checkout-img">
                                        <img src={App_short} alt="App Screen" className="img-fluid" />
                                    </div>
                                    <div className="BP_product-detaile__content credits-img">
                                        <img src={instant_credits} alt="App Screen" className="img-fluid" />
                                    </div>
                                    <div className="BP_product-detaile__content chargebacks-img">
                                        <img src={chargebacks} alt="App Screen" className="img-fluid" />
                                    </div>
                                    <div className="BP_product-detaile__content encryption-img">
                                        <img src={bank_level_encryption} alt="App Screen" className="img-fluid" />
                                    </div>
                                </div>
                            </Container>
                        </div>
                        <div className="BP-scroll_sec1 BP_scrolling-section BP_click-checkout-section">
                            <Container className="">
                                <Row className="align-items-center">
                                    <Col md="6" >
                                        <div className="BP_product-detaile__img d-md-none">
                                            <div className="BP_product-detaile__content">
                                                <img src={App_short} alt="App Screen" className="img-fluid" />
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <div className="product-detaile__content text-right">
                                            <h2 className="BP_title">1-click Checkout</h2>
                                            <p className="BP_description">Bankpay enables a frictionless checkout across its entire network letting your customers zip through checkout boosting conversions by over 200%.</p>
                                            <a href="/#" className="link link-dark">
                                                <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M23.725 5.31498C23.7247 5.31469 23.7245 5.31435 23.7242 5.31406L18.8256 0.281803C18.4586 -0.0951806 17.865 -0.0937777 17.4997 0.285093C17.1344 0.663916 17.1359 1.27664 17.5028 1.65367L20.7918 5.03226H0.9375C0.419719 5.03226 0 5.46552 0 6C0 6.53448 0.419719 6.96774 0.9375 6.96774H20.7917L17.5029 10.3463C17.1359 10.7234 17.1345 11.3361 17.4997 11.7149C17.865 12.0938 18.4587 12.0951 18.8256 11.7182L23.7242 6.68594C23.7245 6.68565 23.7247 6.68531 23.7251 6.68502C24.0922 6.30673 24.0911 5.69202 23.725 5.31498Z" fill="black" />
                                                </svg> Get Started
                                            </a>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        <div className="BP-scroll_sec2 BP_scrolling-section BP_instant-credits-section">
                            <Container className="">
                                <Row className="align-items-center">
                                    <Col md="6">
                                        <div className="BP_product-detaile__content d-md-none">
                                            <img src={instant_credits} alt="Instant Credits" className="img-fluid" />
                                        </div>
                                    </Col>
                                    <Col md="6" className="ml-auto">
                                        <div className="product-detaile__content text-right">
                                            <h2 className="BP_title">Direct, instant credits</h2>
                                            <p className="BP_description">With Bankpay, customers pay directly to you, funds hit your bank account in seconds. No middlemen, no 5-day payout cycles.</p>
                                            <a href="/#" className="link link-dark">
                                                <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M23.725 5.31498C23.7247 5.31469 23.7245 5.31435 23.7242 5.31406L18.8256 0.281803C18.4586 -0.0951806 17.865 -0.0937777 17.4997 0.285093C17.1344 0.663916 17.1359 1.27664 17.5028 1.65367L20.7918 5.03226H0.9375C0.419719 5.03226 0 5.46552 0 6C0 6.53448 0.419719 6.96774 0.9375 6.96774H20.7917L17.5029 10.3463C17.1359 10.7234 17.1345 11.3361 17.4997 11.7149C17.865 12.0938 18.4587 12.0951 18.8256 11.7182L23.7242 6.68594C23.7245 6.68565 23.7247 6.68531 23.7251 6.68502C24.0922 6.30673 24.0911 5.69202 23.725 5.31498Z" fill="black" />
                                                </svg> Get Started
                                            </a>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        <div className="BP-scroll_sec3 BP_scrolling-section BP_chargebacks-section">
                            <Container className="">
                                <Row className="align-items-center">
                                    <Col md="6">
                                        <div className="BP_product-detaile__content d-md-none">
                                            <img src={chargebacks} alt="Instant Credits" className="img-fluid" />
                                        </div>
                                    </Col>
                                    <Col md="6" className="ml-auto">
                                        <div className="product-detaile__content text-right">
                                            <h2 className="BP_title">No disputes or chargebacks</h2>
                                            <p className="BP_description">Open Banking enables customers to authorise payments directly from their bank account. This ensures there are no disputes, ever.</p>
                                            <a href="/#" className="link link-dark">
                                                <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M23.725 5.31498C23.7247 5.31469 23.7245 5.31435 23.7242 5.31406L18.8256 0.281803C18.4586 -0.0951806 17.865 -0.0937777 17.4997 0.285093C17.1344 0.663916 17.1359 1.27664 17.5028 1.65367L20.7918 5.03226H0.9375C0.419719 5.03226 0 5.46552 0 6C0 6.53448 0.419719 6.96774 0.9375 6.96774H20.7917L17.5029 10.3463C17.1359 10.7234 17.1345 11.3361 17.4997 11.7149C17.865 12.0938 18.4587 12.0951 18.8256 11.7182L23.7242 6.68594C23.7245 6.68565 23.7247 6.68531 23.7251 6.68502C24.0922 6.30673 24.0911 5.69202 23.725 5.31498Z" fill="black" />
                                                </svg> Get Started
                                            </a>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        <div className="BP-scroll_sec4 BP_scrolling-section BP_encryption-section">
                            <Container className="">
                                <Row className="align-items-center">
                                    <Col md="6">
                                        <div className="BP_product-detaile__content d-md-none">
                                            <img src={bank_level_encryption} alt="Bank Level Encryption" className="img-fluid" />
                                        </div>
                                    </Col>
                                    <Col md="6" className="ml-auto">
                                        <div className="product-detaile__content text-right">
                                            <h2 className="BP_title">Bank-level encryption</h2>
                                            <p className="BP_description">Heartface and its infrastructure partners employ the strongest security and encryption tech to ensure what’s yours, remains yours.</p>
                                            <a href="/#" className="link link-dark">
                                                <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M23.725 5.31498C23.7247 5.31469 23.7245 5.31435 23.7242 5.31406L18.8256 0.281803C18.4586 -0.0951806 17.865 -0.0937777 17.4997 0.285093C17.1344 0.663916 17.1359 1.27664 17.5028 1.65367L20.7918 5.03226H0.9375C0.419719 5.03226 0 5.46552 0 6C0 6.53448 0.419719 6.96774 0.9375 6.96774H20.7917L17.5029 10.3463C17.1359 10.7234 17.1345 11.3361 17.4997 11.7149C17.865 12.0938 18.4587 12.0951 18.8256 11.7182L23.7242 6.68594C23.7245 6.68565 23.7247 6.68531 23.7251 6.68502C24.0922 6.30673 24.0911 5.69202 23.725 5.31498Z" fill="black" />
                                                </svg> Get Started
                                            </a>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    </section>

                    
                    <section className="BP_customer-loyalty-section">
                        <Container fluid>
                            <Row>
                                <Col lg={7}>
                                    <div className="BP_customer-loyalty__content">
                                        <h3 className="BP_title">Customer loyalty comes built-in with Bankpay</h3>
                                        <div className="BP_description">
                                            <p>Bankpay’s points program rewards your customers for returning to your store and shop more.</p>

                                            <p>Every merchant we sign up is automatically enrolled into our Brand Partner program at no extra cost with an optional suite of offerings to boost customer loyalty.</p>
                                        </div>
                                        <a href="/#" className="link link-dark">
                                            <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M23.725 5.31498C23.7247 5.31469 23.7245 5.31435 23.7242 5.31406L18.8256 0.281803C18.4586 -0.0951806 17.865 -0.0937777 17.4997 0.285093C17.1344 0.663916 17.1359 1.27664 17.5028 1.65367L20.7918 5.03226H0.9375C0.419719 5.03226 0 5.46552 0 6C0 6.53448 0.419719 6.96774 0.9375 6.96774H20.7917L17.5029 10.3463C17.1359 10.7234 17.1345 11.3361 17.4997 11.7149C17.865 12.0938 18.4587 12.0951 18.8256 11.7182L23.7242 6.68594C23.7245 6.68565 23.7247 6.68531 23.7251 6.68502C24.0922 6.30673 24.0911 5.69202 23.725 5.31498Z" fill="black" />
                                            </svg> Get Started
                                        </a>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                        <div className="BP_customer_bg">
                            <img src={CustomersBG} className="img-fluid" alt="Customer Background" />
                        </div>
                    </section>
                    <section className="BP_getStarted-section">
                        <Container>
                            <Row className="justify-content-center">
                                <Col lg={10}>
                                    <div className="BP_getStarted__content">
                                        <img src={Logo} className="img-fluid" alt="Landing Background" />
                                        <div className="BP_text">Offer your customers a faster, safer and more rewarding payment alternative</div>
                                        <a href="/#" className="btn btn-primary">Get Started</a>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </section>
               </main>
               <Footer />
            </Page>
        );
    }
}
