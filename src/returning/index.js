import { useState } from 'react';

// import $ from "jquery";

import './styles.scss';

import { Page } from '@ui-kit';
// import { Container, Navbar,Nav, Form, Button  } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { ReturningModal } from '@ui-kit/components/Modals';

export default function Balance({ amount }) {
  const [showReturningModal, setReturningModal] = useState(false);

  return (
        <Page className="BP_Returning">
            <Button variant="secondary" className="mr-3" onClick={() => setReturningModal(true)}>Checkout</Button>
            <ReturningModal
                show={showReturningModal}
                handleClose={() => setReturningModal(false)}
            />
        </Page>
        
  );
}