import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import './assets/css/main.scss';

import { BrowserRouter, Switch, Route } from 'react-router-dom';
// import LandingPage from './landing-page';
import Payment from './payment';
import Account from './account';
import Returning from './returning';
import Login from './login';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route exact path="/payment">
            <Payment />
          </Route>
          <Route exact path="/account">
            <Account />
          </Route>
          <Route exact path="/returning">
            <Returning />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
