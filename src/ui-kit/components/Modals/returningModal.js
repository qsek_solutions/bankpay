import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Accordion, Button, Card, Col, Form, InputGroup, Row,Dropdown, FormControl, FormGroup } from 'react-bootstrap';
import Modal from 'react-bootstrap/modal';

import hsbc_icon from '../../../assets/images/hsbc-icon.png';
import monzo_icon from '../../../assets/images/monzo-icon.png';
import natwest_icon from '../../../assets/images/natwest-icon.png';

export default function ReturningModal({ show, handleClose,}) {
    // const [IsActive, setIsActive] = useState(false);
    const [shipForm,setshipForm] = useState(false);
    const [shipEmailvalid,setshipEmailvalid] = useState(false);
    const [shipNamecng,setshipNamecng] = useState(false);
    const [addShipadd,setaddShipadd] = useState(false);
    const [CheckoutSuccess,setCheckoutSuccess] = useState(false);
    const [addShipphone,setaddShipphone] = useState(false);
    const [addShipbank,setaddShipbank] = useState(false);
    const [Shipform, setShipform] = useState({
        email: "",
        firstname: "",
        lastname: "",
        address: "",
        phoneNumber: ""
    });
    const [Formname, setFormname] = useState('Urvesh Vasani');
    const [shipCheckotp,setshipCheckotp] = useState(false);

    const [changeShipMethod,setchangeShipMethod] = useState(false);
    const [changeAddress,setchangeAddress] = useState(false);
    const [changePhoneNumber,setchangePhoneNumber] = useState(false);
    const [changeBank,setchangeBank] = useState(false);


    const handleChange = e => {
        if(e.target.name === "email"){
            if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)) {
                setshipEmailvalid(true);
            }else{
                setshipEmailvalid(false);
            }
        }
        setShipform({
            email: "email" === e.target.name ? e.target.value : Shipform.email,
            firstname: "firstname" === e.target.name ? e.target.value : Shipform.firstname,
            lastname: "lastname" === e.target.name ? e.target.value : Shipform.lastname,
            address: "address" === e.target.name ? e.target.value : Shipform.address,
            phoneNumber:"phoneNumber" === e.target.name ? e.target.value : Shipform.phoneNumber
        });
    };


    function shipping_toggle(val) {
        if(!shipForm){
            setshipForm(true);
        } else {
            setshipForm(false);
        }

    }

    // function verifyemail(val) {
    //     if(!shipForm){
    //         setshipForm(true);
    //     } else {
    //         setshipForm(false);
    //     }

    // }

    function clsoepopup() {
        setshipForm(false);
        setshipNamecng(false);
        setaddShipadd(false);
        setCheckoutSuccess(false);
        setaddShipphone(false);
        setaddShipbank(false);
        setshipCheckotp(false);
        setchangeShipMethod(false);
        setchangeAddress(false);
        setchangePhoneNumber(false);
    }
    
    return (
        <Modal
            show={show}
            centered
            aria-labelledby="contained-modal-title-vcenter"
            dialogClassName="returning-modal"
            id="returning_balance"
            onHide={handleClose}
        >
            <div className={"returning-modal__content "+(CheckoutSuccess ? 'd-none' : 'd-block')}>
                <Modal.Header>
                    <div className="modal-logo">
                        <svg width="160" height="16" viewBox="0 0 160 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.119 7.63429C20.5301 7.98476 21.6226 8.49524 22.3965 9.16571C23.1704 9.83619 23.5573 10.6438 23.5573 11.5886C23.5573 12.9905 22.6924 14.08 20.9626 14.8571C19.2328 15.619 16.7291 16 13.4516 16H0V0H12.7687C15.9552 0 18.3565 0.380952 19.9725 1.14286C21.6113 1.90476 22.4306 2.9181 22.4306 4.18286C22.4306 4.92952 22.1461 5.6 21.5771 6.19429C21.0309 6.78857 20.2115 7.26857 19.119 7.63429ZM7.92071 3.77143V6.14857H11.6762C13.4288 6.14857 14.3051 5.75238 14.3051 4.96C14.3051 4.16762 13.4288 3.77143 11.6762 3.77143H7.92071ZM12.7687 12.2286C14.5441 12.2286 15.4317 11.8095 15.4317 10.9714C15.4317 10.1333 14.5441 9.71429 12.7687 9.71429H7.92071V12.2286H12.7687Z" fill="white"/>
                            <path d="M39.2771 13.2114H30.1956L28.591 16H20.3971L30.8443 0H38.765L49.2121 16H40.8817L39.2771 13.2114ZM37.0579 9.32571L34.7363 5.30286L32.4148 9.32571H37.0579Z" fill="white"/>
                            <path d="M70.6412 0V16H64.0179L54.9364 8.73143V16H47.0839V0H53.7073L62.7888 7.26857V0H70.6412Z" fill="white"/>
                            <path d="M80.8395 10.6971L79.0301 12.0457V16H71.1093V0H79.0301V5.80571L86.8825 0H95.6567L86.0972 7.04L96.1347 16H86.8142L80.8395 10.6971Z" fill="white"/>
                            <path d="M105.307 0C107.514 0 109.438 0.24381 111.076 0.731429C112.715 1.21905 113.978 1.92 114.866 2.83429C115.754 3.74857 116.198 4.81524 116.198 6.03429C116.198 7.25333 115.754 8.32 114.866 9.23429C113.978 10.1486 112.715 10.8495 111.076 11.3371C109.438 11.8248 107.514 12.0686 105.307 12.0686H101.858V16H93.8011V0H105.307ZM104.795 7.90857C105.887 7.90857 106.706 7.74857 107.253 7.42857C107.799 7.09333 108.072 6.62857 108.072 6.03429C108.072 5.44 107.799 4.98286 107.253 4.66286C106.706 4.32762 105.887 4.16 104.795 4.16H101.858V7.90857H104.795Z" fill="white"/>
                            <path d="M130.511 13.2114H121.429L119.825 16H111.631L122.078 0H129.999L140.446 16H132.116L130.511 13.2114ZM128.292 9.32571L125.97 5.30286L123.649 9.32571H128.292Z" fill="white"/>
                            <path d="M150.987 10.1943V16H142.929V10.1257L133.916 0H142.417L147.3 5.53143L152.216 0H160L150.987 10.1943Z" fill="white"/>
                        </svg>
                    </div>
                    {addShipadd || addShipphone ? (
                        <a href="#/" className="close-popup" onClick={(e) => clsoepopup()}>Cancel</a>
                    ) : (
                        <span className="checkout-price">
                            £120.00
                            <span className="quantity">2</span>
                        </span>
                    )}
                </Modal.Header>
                <div className="modal-points-label">
                    THIS PURCHASE WILL EARN YOU 188 POINTS
                </div>
                <Modal.Body>
                    <div className={"returning-modal__wrapper "+(addShipadd || addShipphone || addShipbank || shipCheckotp ? 'd-none' : 'd-block')}>
                        <Accordion className="shipping-accordion">  
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0" onClick={(e) => shipping_toggle(e.target.value)}>
                                    Standard Shipping (free, 3-5 days)
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="0">
                                    <Card.Body>
                                        <Form className="std-shipping-form">
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Control type="email" value={Shipform.email} name="email" placeholder="Email" onChange={handleChange}/>
                                                <Form.Label className={ Shipform.email ? "floating" : ""}>Email</Form.Label>
                                                {shipEmailvalid  &&
                                                    <Button type="button" variant="email-varify" onClick={(e) => setshipCheckotp(true)}>
                                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M3.61636 4.02202L2.71013 4.92825C3.60227 2.00497 6.69528 0.358431 9.61853 1.25056C10.7326 1.59056 11.711 2.27341 12.4143 3.20185L13.2164 2.59418C11.0381 -0.285559 6.93773 -0.85425 4.05796 1.32404C2.86435 2.22687 2.01665 3.51189 1.65639 4.9645L0.711894 4.02202L0 4.73341L2.16488 6.8983L4.32977 4.73341L3.61636 4.02202Z" fill="white"/>
                                                            <path d="M13.8352 6.18237L11.6703 8.34726L12.3822 9.05915L13.299 8.14285C12.4147 11.0708 9.32424 12.7276 6.39626 11.8433C5.23166 11.4915 4.2153 10.7661 3.50413 9.7791L2.68701 10.3671C3.69686 11.779 5.22908 12.7286 6.94277 13.0048C7.294 13.0618 7.64925 13.0906 8.00507 13.0909C11.0107 13.0895 13.6271 11.0366 14.3436 8.11768L15.2861 9.05915L16 8.34726L13.8352 6.18237ZM13.81 7.63285L13.8357 7.60717L13.8729 7.64493L13.81 7.63285Z" fill="white"/>
                                                        </svg>
                                                        <svg className="d-none" width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M15.7147 0.286047C15.3334 -0.0953283 14.7162 -0.0953595 14.3349 0.286016L5.12156 9.49935L1.66512 6.0429C1.28384 5.66153 0.666618 5.66149 0.285305 6.04287C-0.0951017 6.42331 -0.0951017 7.04228 0.285305 7.42266L4.43169 11.5691C4.81291 11.9503 5.43022 11.9503 5.81147 11.5691L15.7147 1.6658C16.0951 1.28539 16.0951 0.666392 15.7147 0.286047Z" fill="#B7E183"/>
                                                            <path d="M15.7148 0.286047C15.3336 -0.0953283 14.7163 -0.0953595 14.335 0.286016L7.60986 7.01119V9.77075L15.7148 1.6658C16.0952 1.28539 16.0952 0.666392 15.7148 0.286047Z" fill="#71DE56"/>
                                                        </svg>
                                                    </Button>
                                                }
                                            </Form.Group>
                                            <Form.Row>
                                                <Col>
                                                    <Form.Group controlId="firstName" className={( !shipEmailvalid ? 'disabled' : '')}>
                                                        <Form.Control type="text" value={Shipform.firstname} name="firstname" disabled={( !shipEmailvalid ? true : false)} placeholder="First Name" onChange={handleChange}/>
                                                        <Form.Label className={ Shipform.firstname ? "floating" : ""}>First Name</Form.Label>
                                                    </Form.Group>
                                                </Col>
                                                <Col>
                                                    <Form.Group controlId="lastName" className={( !shipEmailvalid ? 'disabled' : '')}>
                                                        <Form.Control type="text" value={Shipform.lastname} name="lastname"  disabled={( !shipEmailvalid ? true : false)} placeholder="Last Name" onChange={handleChange}/>
                                                        <Form.Label className={ Shipform.lastname ? "floating" : ""}>Last Name</Form.Label>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Row>
                                            <InputGroup className={"mb-3 "+( !shipEmailvalid ? 'disabled' : '')} controlId="shippingAddress">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text>
                                                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M11.6832 10.1509L9.54123 8.01395C9.53043 8.00315 9.51771 7.99523 9.50643 7.98755C9.31395 8.28347 9.0838 8.56474 8.82412 8.82442C8.56468 9.0841 8.28292 9.31426 7.98389 9.50721C7.99445 9.51825 8.00237 9.53121 8.01317 9.54177L10.1508 11.6835C10.5739 12.1059 11.2611 12.1054 11.6827 11.6835C12.1049 11.2611 12.1051 10.5738 11.6832 10.1509Z" fill="#ffffff"/>
                                                            <path d="M8.51892 1.45901C6.57302 -0.486415 3.40746 -0.486415 1.46132 1.45949C-0.486745 3.40491 -0.486985 6.57288 1.45988 8.51902C3.40674 10.4642 6.57302 10.4642 8.51892 8.51878C10.4648 6.57288 10.4646 3.40467 8.51892 1.45901ZM2.37715 7.6027C0.936441 6.162 0.936921 3.8165 2.37763 2.37724C3.81833 0.936771 6.16239 0.936771 7.60261 2.37652C9.04236 3.8165 9.04188 6.15936 7.60261 7.60222C6.16215 9.03837 3.81833 9.04365 2.37715 7.6027Z" fill="#ffffff"/>
                                                            <path d="M3.4035 5.39105C3.29766 4.95617 2.86182 4.35282 2.49511 4.54506C2.20447 4.69338 2.28631 5.37353 2.42335 5.77577C3.03582 7.60479 5.26324 7.99142 5.49484 7.76007C5.7226 7.53255 3.82302 7.11207 3.4035 5.39105Z" fill="#ffffff"/>
                                                        </svg>
                                                    </InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <Form.Control type="text" value={Shipform.address} name="address" disabled={( !shipEmailvalid ? true : false)} placeholder="Start typing your shipping address" onChange={handleChange}/>
                                                <Form.Label className={ Shipform.address ? "floating" : ""}>Shipping Address</Form.Label>
                                                <div className="search-result" style={{display: "none"}}>
                                                    <ul className="list-unstyled">
                                                        <li><a href="#/">Flat 106, The Oxygen, 18</a></li>    
                                                        <li><a href="#/">Flat 106, The Oxygen, 19</a></li>    
                                                        <li><a href="#/">Flat 106, The Oxygen, 20</a></li>    
                                                    </ul>                                                    
                                                </div>
                                            </InputGroup>
                                            <Form.Group controlId="phoneNumber"  className={( !shipEmailvalid ? 'disabled' : '')}>
                                                <Form.Control type="text" value={Shipform.phoneNumber} name="phoneNumber" disabled={( !shipEmailvalid ? true : false)} placeholder="Phone number" onChange={handleChange}/>
                                                <Form.Label className={ Shipform.phoneNumber ? "floating" : ""}>Phone number</Form.Label>
                                            </Form.Group>
                                            <Form.Group controlId="formBasicCheckbox" className="mb-0">
                                            {['checkbox'].map((type) => (
                                                <div key={`custom-${type}`}>
                                                    <Form.Check 
                                                        custom
                                                        type={type}
                                                        id={`custom-${type}`}
                                                        label={`Check this custom ${type}`}
                                                    />
                                                </div>
                                            ))}
                                            </Form.Group>
                                        </Form>
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                        <div className={( shipForm ? 'd-none' : 'd-block')}>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Name</label>
                                </Col>
                                <Col xs={9}>
                                    <div className="modal-form__group">
                                        <input type="text" className="modal-form__input" readOnly={( !shipNamecng ? true : false)} value={Formname} onChange={(e) => setFormname(e.target.value)}  />
                                        <a href="#/" className="link link-white" onClick={(e) => shipNamecng ? setshipNamecng(false) : setshipNamecng(true)}>{shipNamecng ? "Save" : "Edit"}</a>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Ship to</label>
                                </Col>
                                <Col xs={9}>
                                    <Dropdown>
                                        <Dropdown.Toggle id="dropdown-ship" className="detail-control">Flat 106, The Oxygen - E16 1BQ</Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#/" className="selected">Flat 106, The Oxygen, 18 Western Gateway - E16 1BQ</Dropdown.Item>
                                            <Dropdown.Item href="#/" className="add_new" onClick={(e) => addShipadd ? setaddShipadd(false) : setaddShipadd(true)}>Add new...</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Col>
                            </Row>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Phone</label>
                                </Col>
                                <Col xs={9}>
                                    <Dropdown>
                                        <Dropdown.Toggle id="dropdown-phone" className="detail-control">+44 7539227305</Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#/" className="selected">+44 7539227305</Dropdown.Item>
                                            <Dropdown.Item href="#/" className="add_new" onClick={(e) => addShipphone ? setaddShipphone(false) : setaddShipphone(true)}>Add new...</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Col>
                            </Row>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Bank</label>
                                </Col>
                                <Col xs={9}>
                                    <Dropdown className="bankAccount-dropdown">
                                        <Dropdown.Toggle id="dropdown-bankAccount" className="detail-control"><img src={hsbc_icon} alt="HSBC Icon" className="img-fluid" /> HSBC x-4231 - E16 1BQ</Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <span className="dropdown-label">Bank Account</span>
                                            <Dropdown.Item href="#/" className="selected">HSBC x-4231</Dropdown.Item>
                                            <Dropdown.Item href="#/"  className="add_new" onClick={(e) => addShipbank ? setaddShipbank(false) : setaddShipbank(true)}>Use other</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Col>
                            </Row>      
                        </div>              
                        <Button variant="black" onClick={(e) => CheckoutSuccess ? setCheckoutSuccess(false) : setCheckoutSuccess(true)} className="m-0 w-100 btn-checkout">
                            <svg className="ml-0 mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.7081 5.69319C11.5136 5.53409 11.2776 5.45459 10.9997 5.45459H10.6665V3.81814C10.6665 2.7727 10.2083 1.87505 9.29158 1.12505C8.37493 0.375017 7.2779 0 5.99995 0C4.72202 0 3.62478 0.375017 2.7082 1.12502C1.79148 1.87505 1.33327 2.77267 1.33327 3.81814V5.45459H0.999979C0.722292 5.45459 0.486125 5.53409 0.29166 5.69319C0.0971958 5.85218 0 6.0454 0 6.27281V11.1818C0 11.409 0.0972323 11.6023 0.29166 11.7615C0.486125 11.9204 0.722292 12 0.999979 12H10.9999C11.2778 12 11.5138 11.9205 11.7083 11.7615C11.9026 11.6023 12 11.409 12 11.1818V6.27272C12.0001 6.04549 11.9026 5.8523 11.7081 5.69319ZM8.66659 5.45459H3.3333V3.81814C3.3333 3.2159 3.59376 2.70166 4.11459 2.27559C4.63548 1.84947 5.2639 1.63646 6.00006 1.63646C6.73628 1.63646 7.36455 1.84944 7.88549 2.27559C8.40616 2.70163 8.66659 3.2159 8.66659 3.81814V5.45459Z" fill="white"/>
                            </svg>
                            Checkout
                        </Button>
                    </div>
                    <div className="returning-modal__wrapper_mobile">
                        <Button type="button" variant="std-shipping" onClick={(e) => changeShipMethod ? setchangeShipMethod(false) : setchangeShipMethod(true)}>Standard Shipping (free, 3-5 days)</Button>

                        <Form className="std-shipping-form d-none">
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control type="email" value={Shipform.email} name="email" placeholder="Email" onChange={handleChange}/>
                                <Form.Label className={ Shipform.email ? "floating" : ""}>Email</Form.Label>
                                {shipEmailvalid  &&
                                    <Button type="button" variant="email-varify" onClick={(e) => setshipCheckotp(true)}>
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.61636 4.02202L2.71013 4.92825C3.60227 2.00497 6.69528 0.358431 9.61853 1.25056C10.7326 1.59056 11.711 2.27341 12.4143 3.20185L13.2164 2.59418C11.0381 -0.285559 6.93773 -0.85425 4.05796 1.32404C2.86435 2.22687 2.01665 3.51189 1.65639 4.9645L0.711894 4.02202L0 4.73341L2.16488 6.8983L4.32977 4.73341L3.61636 4.02202Z" fill="white"/>
                                            <path d="M13.8352 6.18237L11.6703 8.34726L12.3822 9.05915L13.299 8.14285C12.4147 11.0708 9.32424 12.7276 6.39626 11.8433C5.23166 11.4915 4.2153 10.7661 3.50413 9.7791L2.68701 10.3671C3.69686 11.779 5.22908 12.7286 6.94277 13.0048C7.294 13.0618 7.64925 13.0906 8.00507 13.0909C11.0107 13.0895 13.6271 11.0366 14.3436 8.11768L15.2861 9.05915L16 8.34726L13.8352 6.18237ZM13.81 7.63285L13.8357 7.60717L13.8729 7.64493L13.81 7.63285Z" fill="white"/>
                                        </svg>
                                        <svg className="d-none" width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.7147 0.286047C15.3334 -0.0953283 14.7162 -0.0953595 14.3349 0.286016L5.12156 9.49935L1.66512 6.0429C1.28384 5.66153 0.666618 5.66149 0.285305 6.04287C-0.0951017 6.42331 -0.0951017 7.04228 0.285305 7.42266L4.43169 11.5691C4.81291 11.9503 5.43022 11.9503 5.81147 11.5691L15.7147 1.6658C16.0951 1.28539 16.0951 0.666392 15.7147 0.286047Z" fill="#B7E183"/>
                                            <path d="M15.7148 0.286047C15.3336 -0.0953283 14.7163 -0.0953595 14.335 0.286016L7.60986 7.01119V9.77075L15.7148 1.6658C16.0952 1.28539 16.0952 0.666392 15.7148 0.286047Z" fill="#71DE56"/>
                                        </svg>
                                    </Button>
                                }
                            </Form.Group>
                            <Form.Row>
                                <Col>
                                    <Form.Group controlId="firstName" className={( !shipEmailvalid ? 'disabled' : '')}>
                                        <Form.Control type="text" value={Shipform.firstname} name="firstname" disabled={( !shipEmailvalid ? true : false)} placeholder="First Name" onChange={handleChange}/>
                                        <Form.Label className={ Shipform.firstname ? "floating" : ""}>First Name</Form.Label>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group controlId="lastName" className={( !shipEmailvalid ? 'disabled' : '')}>
                                        <Form.Control type="text" value={Shipform.lastname} name="lastname"  disabled={( !shipEmailvalid ? true : false)} placeholder="Last Name" onChange={handleChange}/>
                                        <Form.Label className={ Shipform.lastname ? "floating" : ""}>Last Name</Form.Label>
                                    </Form.Group>
                                </Col>
                            </Form.Row>
                            <InputGroup className={"mb-3 "+( !shipEmailvalid ? 'disabled' : '')} controlId="shippingAddress">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M11.6832 10.1509L9.54123 8.01395C9.53043 8.00315 9.51771 7.99523 9.50643 7.98755C9.31395 8.28347 9.0838 8.56474 8.82412 8.82442C8.56468 9.0841 8.28292 9.31426 7.98389 9.50721C7.99445 9.51825 8.00237 9.53121 8.01317 9.54177L10.1508 11.6835C10.5739 12.1059 11.2611 12.1054 11.6827 11.6835C12.1049 11.2611 12.1051 10.5738 11.6832 10.1509Z" fill="#ffffff"/>
                                            <path d="M8.51892 1.45901C6.57302 -0.486415 3.40746 -0.486415 1.46132 1.45949C-0.486745 3.40491 -0.486985 6.57288 1.45988 8.51902C3.40674 10.4642 6.57302 10.4642 8.51892 8.51878C10.4648 6.57288 10.4646 3.40467 8.51892 1.45901ZM2.37715 7.6027C0.936441 6.162 0.936921 3.8165 2.37763 2.37724C3.81833 0.936771 6.16239 0.936771 7.60261 2.37652C9.04236 3.8165 9.04188 6.15936 7.60261 7.60222C6.16215 9.03837 3.81833 9.04365 2.37715 7.6027Z" fill="#ffffff"/>
                                            <path d="M3.4035 5.39105C3.29766 4.95617 2.86182 4.35282 2.49511 4.54506C2.20447 4.69338 2.28631 5.37353 2.42335 5.77577C3.03582 7.60479 5.26324 7.99142 5.49484 7.76007C5.7226 7.53255 3.82302 7.11207 3.4035 5.39105Z" fill="#ffffff"/>
                                        </svg>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control type="text" value={Shipform.address} name="address" disabled={( !shipEmailvalid ? true : false)} placeholder="Start typing your shipping address" onChange={handleChange}/>
                                <Form.Label className={ Shipform.address ? "floating" : ""}>Shipping Address</Form.Label>
                                <div className="search-result" style={{display: "none"}}>
                                    <ul className="list-unstyled">
                                        <li><a href="#/">Flat 106, The Oxygen, 18</a></li>    
                                        <li><a href="#/">Flat 106, The Oxygen, 19</a></li>    
                                        <li><a href="#/">Flat 106, The Oxygen, 20</a></li>    
                                    </ul>                                                    
                                </div>
                            </InputGroup>
                            <Form.Group controlId="phoneNumber"  className={( !shipEmailvalid ? 'disabled' : '')}>
                                <Form.Control type="text" value={Shipform.phoneNumber} name="phoneNumber" disabled={( !shipEmailvalid ? true : false)} placeholder="Phone number" onChange={handleChange}/>
                                <Form.Label className={ Shipform.phoneNumber ? "floating" : ""}>Phone number</Form.Label>
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox" className="mb-0">
                            {['checkbox'].map((type) => (
                                <div key={`custom-${type}`}>
                                    <Form.Check 
                                        custom
                                        type={type}
                                        id={`custom-${type}`}
                                        label={`Check this custom ${type}`}
                                    />
                                </div>
                            ))}
                            </Form.Group>
                        </Form>
                        <div className="d-block">
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Name</label>
                                </Col>
                                <Col xs={9}>
                                    <div className="modal-form__group">
                                        <input type="text" className="modal-form__input" readOnly={( !shipNamecng ? true : false)} value={Formname} onChange={(e) => setFormname(e.target.value)}  />
                                        <a href="#/" className="link link-white" onClick={(e) => shipNamecng ? setshipNamecng(false) : setshipNamecng(true)}>{shipNamecng ? "Save" : "Edit"}</a>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Ship to</label>
                                </Col>
                                <Col xs={9}>
                                    <Button type="button" onClick={(e) => changeAddress ? setchangeAddress(false) : setchangeAddress(true)} className="detail-control">Flat 106, The Oxygen - E16 1BQ</Button>
                                </Col>
                            </Row>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Phone</label>
                                </Col>
                                <Col xs={9}>
                                    <Button type="button" onClick={(e) => changePhoneNumber ? setchangePhoneNumber(false) : setchangePhoneNumber(true)} className="detail-control">+44 7539227305</Button>
                                </Col>
                            </Row>
                            <Row className="align-items-center mb-3">
                                <Col xs={3}>
                                    <label className="modal-form-label">Bank</label>
                                </Col>
                                <Col xs={9}>
                                    <Button type="button" onClick={(e) => changeBank ? setchangeBank(false) : setchangeBank(true)} className="detail-control"><img src={hsbc_icon} alt="HSBC Icon" className="img-fluid" /> HSBC x-4231 - E16 1BQ</Button>
                                </Col>
                            </Row>  
                        </div>              
                        <Button variant="black" onClick={(e) => CheckoutSuccess ? setCheckoutSuccess(false) : setCheckoutSuccess(true)} className="m-0 w-100 btn-checkout">
                            <svg className="ml-0 mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.7081 5.69319C11.5136 5.53409 11.2776 5.45459 10.9997 5.45459H10.6665V3.81814C10.6665 2.7727 10.2083 1.87505 9.29158 1.12505C8.37493 0.375017 7.2779 0 5.99995 0C4.72202 0 3.62478 0.375017 2.7082 1.12502C1.79148 1.87505 1.33327 2.77267 1.33327 3.81814V5.45459H0.999979C0.722292 5.45459 0.486125 5.53409 0.29166 5.69319C0.0971958 5.85218 0 6.0454 0 6.27281V11.1818C0 11.409 0.0972323 11.6023 0.29166 11.7615C0.486125 11.9204 0.722292 12 0.999979 12H10.9999C11.2778 12 11.5138 11.9205 11.7083 11.7615C11.9026 11.6023 12 11.409 12 11.1818V6.27272C12.0001 6.04549 11.9026 5.8523 11.7081 5.69319ZM8.66659 5.45459H3.3333V3.81814C3.3333 3.2159 3.59376 2.70166 4.11459 2.27559C4.63548 1.84947 5.2639 1.63646 6.00006 1.63646C6.73628 1.63646 7.36455 1.84944 7.88549 2.27559C8.40616 2.70163 8.66659 3.2159 8.66659 3.81814V5.45459Z" fill="white"/>
                            </svg>
                            Checkout
                        </Button>
                    </div>
                    <div className={"edited-form add-phoneNumber_form  "+(addShipphone ? 'd-block' : 'd-none')}>
                        <label className="text-center d-block edited-form__label">Add a new phone number</label>
                        <Row className="align-items-center mb-3">
                            <Col xs={3}>
                                <Form.Label className="modal-form-label">Phone</Form.Label>
                            </Col>
                            <Col xs={6}>
                                <FormControl value="+44 7539227305" />
                            </Col>
                        </Row>
                        <div className="edited-form__action d-flex justify-content-end">
                            <a href="#/" className="link link-white" onClick={(e) => setaddShipphone(false)}>Cancel</a>
                            <a href="#/" className="link link-white" onClick={(e) => setaddShipphone(false)}>Save</a>
                        </div>
                    </div>
                    <div className={"edited-form add-phoneNumber_form "+(addShipadd ? 'd-block' : 'd-none')}>
                        <label className="text-center d-block edited-form__label">Add a new shipping address</label>
                        <Form.Group controlId="formAddress">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" placeholder="123 Main Street"/>
                        </Form.Group>
                        <Row>
                            <Col>
                                <Form.Group controlId="formAddress">
                                    <Form.Label>Post Code</Form.Label>
                                    <Form.Control type="text" placeholder="A9 9AA"/>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group controlId="formAddress">
                                    <Form.Label>City/Town</Form.Label>
                                    <Form.Control type="text" placeholder="London"/>
                                </Form.Group>
                            </Col>
                        </Row>
                        <div className="edited-form__action d-flex justify-content-end">
                            <a href="#/" className="link link-white" onClick={(e) => setaddShipadd(false)}>Cancel</a>
                            <a href="#/" className="link link-white" onClick={(e) => setaddShipadd(false)}>Save</a>
                        </div>
                    </div>
                    <div className={"edited-form bank-account-list  "+(addShipbank ? 'd-block' : 'd-none')}>
                        <div className="bank-account__item">
                            <a href="#/" className="bank-account__link">
                                <img src={hsbc_icon} alt="HSBC Icon" className="img-fluid" /> HSBC x-4231
                            </a>
                        </div>
                        <div className="bank-account__item"></div>
                    </div>
                    <div className={"otp-form "+(shipForm && shipCheckotp ? 'd-block' : 'd-none')}>
                        <h4 className="otp-form__title">CONFIRM EMAIL</h4>
                        <p className="otp-form__description">Welcome back! <br/> Check your email for a verification code and enter it below.</p>
                        <a href="#/" className="otp-action-link">Resend verification code</a>
                        <div className="otpField-group digit-group">
                            <input type="password" id="digit-1" className="" name="digit-1" data-next="digit-2" maxLength="1" />
                            <input type="password" id="digit-2" className="" name="digit-2" data-next="digit-3" maxLength="1" data-previous="digit-1" />
                            <input type="password" id="digit-3" className="" name="digit-3" data-next="digit-4" maxLength="1" data-previous="digit-2" />
                            <input type="password" id="digit-4" className="" name="digit-4" data-next="digit-5" maxLength="1" data-previous="digit-3" />
                        </div>
                        <Button variant="black" className="w-100 btn-continue" onClick={(e) => {clsoepopup(); setshipForm(true);}}>
                            Continue
                            <svg className="d-none" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M17.3046 8.19232C17.6708 8.55853 17.6708 9.15216 17.3046 9.51819L11.0153 15.8077C10.649 16.1737 10.0556 16.1737 9.68939 15.8077L6.69543 12.8135C6.32922 12.4475 6.32922 11.8539 6.69543 11.4879C7.06146 11.1216 7.65509 11.1216 8.02112 11.4879L10.3522 13.819L15.9787 8.19232C16.3449 7.82629 16.9385 7.82629 17.3046 8.19232ZM24 12C24 18.633 18.6321 24 12 24C5.367 24 0 18.6321 0 12C0 5.367 5.36792 0 12 0C18.633 0 24 5.36792 24 12ZM22.125 12C22.125 6.40338 17.5959 1.875 12 1.875C6.40338 1.875 1.875 6.40411 1.875 12C1.875 17.5966 6.40411 22.125 12 22.125C17.5966 22.125 22.125 17.5959 22.125 12Z" fill="white"/>
                            </svg>
                        </Button>
                    </div>
                
                    <div className={"edited-form__mobile changeShip-form "+(changeShipMethod ? 'd-block' : 'd-none')}>
                        <div className="edited-form__mobile_header">
                            <a href="#/" className="link-back" onClick={(e) => changeShipMethod ? setchangeShipMethod(false) : setchangeShipMethod(true)}>
                                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M-3.4969e-07 7.99997C-3.37156e-07 7.71322 0.0960371 7.4265 0.287709 7.20788L6.32224 0.328227C6.70612 -0.109409 7.3285 -0.109409 7.71221 0.328227C8.09593 0.765686 8.09593 1.4751 7.71221 1.91277L2.37252 7.99997L7.71203 14.0872C8.09574 14.5249 8.09574 15.2342 7.71203 15.6716C7.32831 16.1095 6.70593 16.1095 6.32206 15.6716L0.287522 8.79207C0.0958196 8.57334 -3.6222e-07 8.28662 -3.4969e-07 7.99997Z" fill="white"/>
                                </svg>
                                Back
                            </a>
                            <h3 className="title">Change Shipping Method</h3>
                        </div>
                        <div className="edited-form__mobile_body">
                            <ul className="list-unstyled edited-selected-list">
                                <li className="">
                                    <a href="#/" className="selected">Standard Shipping (free, 3-5 days)</a>
                                </li>
                                <li className="">
                                    <a href="#/">Express Shipping (£5, next day)</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className={"edited-form__mobile changeAddress-form "+(changeAddress ? 'd-block' : 'd-none')}>
                        <div className="edited-form__mobile_header">
                            <a href="#/" className="link-back" onClick={(e) => changeAddress ? setchangeAddress(false) : setchangeAddress(true)}>
                                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M-3.4969e-07 7.99997C-3.37156e-07 7.71322 0.0960371 7.4265 0.287709 7.20788L6.32224 0.328227C6.70612 -0.109409 7.3285 -0.109409 7.71221 0.328227C8.09593 0.765686 8.09593 1.4751 7.71221 1.91277L2.37252 7.99997L7.71203 14.0872C8.09574 14.5249 8.09574 15.2342 7.71203 15.6716C7.32831 16.1095 6.70593 16.1095 6.32206 15.6716L0.287522 8.79207C0.0958196 8.57334 -3.6222e-07 8.28662 -3.4969e-07 7.99997Z" fill="white"/>
                                </svg>
                                Back
                            </a>
                            <h3 className="title">Change Address</h3>
                        </div>
                        <div className="edited-form__mobile_body">
                            <ul className="list-unstyled edited-selected-list">
                                <li className="">
                                    <a href="#/" className="selected">Flat 106, The Oxygen, 18 Western Gateway London E16 1BQ - United Kingdom</a>
                                </li>
                            </ul>
                            <div className="edited-form__mobile_wrapper">
                                <Form>
                                    <FormGroup>
                                        <Form.Label>Add new</Form.Label>
                                        <InputGroup className="mb-3" controlId="post_code">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>
                                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11.6832 10.1509L9.54123 8.01395C9.53043 8.00315 9.51771 7.99523 9.50643 7.98755C9.31395 8.28347 9.0838 8.56474 8.82412 8.82442C8.56468 9.0841 8.28292 9.31426 7.98389 9.50721C7.99445 9.51825 8.00237 9.53121 8.01317 9.54177L10.1508 11.6835C10.5739 12.1059 11.2611 12.1054 11.6827 11.6835C12.1049 11.2611 12.1051 10.5738 11.6832 10.1509Z" fill="#ffffff"/>
                                                        <path d="M8.51892 1.45901C6.57302 -0.486415 3.40746 -0.486415 1.46132 1.45949C-0.486745 3.40491 -0.486985 6.57288 1.45988 8.51902C3.40674 10.4642 6.57302 10.4642 8.51892 8.51878C10.4648 6.57288 10.4646 3.40467 8.51892 1.45901ZM2.37715 7.6027C0.936441 6.162 0.936921 3.8165 2.37763 2.37724C3.81833 0.936771 6.16239 0.936771 7.60261 2.37652C9.04236 3.8165 9.04188 6.15936 7.60261 7.60222C6.16215 9.03837 3.81833 9.04365 2.37715 7.6027Z" fill="#ffffff"/>
                                                        <path d="M3.4035 5.39105C3.29766 4.95617 2.86182 4.35282 2.49511 4.54506C2.20447 4.69338 2.28631 5.37353 2.42335 5.77577C3.03582 7.60479 5.26324 7.99142 5.49484 7.76007C5.7226 7.53255 3.82302 7.11207 3.4035 5.39105Z" fill="#ffffff"/>
                                                    </svg>
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control type="text" name="post_code" placeholder="Post Code"/>
                                            <div className="search-result" style={{display: "none"}}>
                                                <ul className="list-unstyled">
                                                    <li><a href="#/">Flat 106, The Oxygen, 18</a></li>    
                                                    <li><a href="#/">Flat 106, The Oxygen, 19</a></li>    
                                                    <li><a href="#/">Flat 106, The Oxygen, 20</a></li>    
                                                </ul>                                                    
                                            </div>
                                        </InputGroup>
                                    </FormGroup>
                                    <div className="text-center">
                                        <Button variant="black" className="btn-save">Save</Button>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>

                    <div className={"edited-form__mobile changePhoneNumber-form "+(changePhoneNumber ? 'd-block' : 'd-none')}>
                        <div className="edited-form__mobile_header">
                            <a href="#/" className="link-back" onClick={(e) => changePhoneNumber ? setchangePhoneNumber(false) : setchangeAddress(true)}>
                                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M-3.4969e-07 7.99997C-3.37156e-07 7.71322 0.0960371 7.4265 0.287709 7.20788L6.32224 0.328227C6.70612 -0.109409 7.3285 -0.109409 7.71221 0.328227C8.09593 0.765686 8.09593 1.4751 7.71221 1.91277L2.37252 7.99997L7.71203 14.0872C8.09574 14.5249 8.09574 15.2342 7.71203 15.6716C7.32831 16.1095 6.70593 16.1095 6.32206 15.6716L0.287522 8.79207C0.0958196 8.57334 -3.6222e-07 8.28662 -3.4969e-07 7.99997Z" fill="white"/>
                                </svg>
                                Back
                            </a>
                            <h3 className="title">Change Phone Number</h3>
                        </div>
                        <div className="edited-form__mobile_body">
                            <ul className="list-unstyled edited-selected-list">
                                <li className="">
                                    <a href="#/" className="selected">+44 753 922 7305</a>
                                </li>
                            </ul>
                            <div className="edited-form__mobile_wrapper">
                                <Form>
                                    <FormGroup>
                                        <Form.Label>Add new</Form.Label>
                                        <InputGroup className="mb-3 add-phoneNumber" controlId="phone_number_mobile">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text> +44 </InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control type="text" name="phone_number_mobile"/>
                                        </InputGroup>
                                    </FormGroup>
                                    <div className="text-center">
                                        <Button variant="black" className="btn-save">Save</Button>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>

                    <div className={"edited-form__mobile changeBank-form "+(changeBank ? 'd-block' : 'd-none')}>
                        <div className="edited-form__mobile_header">
                            <a href="#/" className="link-back" onClick={(e) => changeBank ? setchangeBank(false) : setchangeAddress(true)}>
                                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M-3.4969e-07 7.99997C-3.37156e-07 7.71322 0.0960371 7.4265 0.287709 7.20788L6.32224 0.328227C6.70612 -0.109409 7.3285 -0.109409 7.71221 0.328227C8.09593 0.765686 8.09593 1.4751 7.71221 1.91277L2.37252 7.99997L7.71203 14.0872C8.09574 14.5249 8.09574 15.2342 7.71203 15.6716C7.32831 16.1095 6.70593 16.1095 6.32206 15.6716L0.287522 8.79207C0.0958196 8.57334 -3.6222e-07 8.28662 -3.4969e-07 7.99997Z" fill="white"/>
                                </svg>
                                Back
                            </a>
                            <h3 className="title">Change Bank</h3>
                        </div>
                        <div className="edited-form__mobile_body">
                            <ul className="list-unstyled edited-selected-list">
                                <li className="">
                                    <a href="#/" className="selected"><img src={hsbc_icon} alt="HSBC Icon" className="img-fluid" /> HSBC x-4231</a>
                                </li>
                            </ul>
                            <div className="edited-form__mobile_wrapper">
                                <Form>
                                    <FormGroup>
                                        <Form.Label>Choose other</Form.Label>
                                        <InputGroup className="mb-3 yourBank-group" controlId="your_bank">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>
                                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11.6832 10.1509L9.54123 8.01395C9.53043 8.00315 9.51771 7.99523 9.50643 7.98755C9.31395 8.28347 9.0838 8.56474 8.82412 8.82442C8.56468 9.0841 8.28292 9.31426 7.98389 9.50721C7.99445 9.51825 8.00237 9.53121 8.01317 9.54177L10.1508 11.6835C10.5739 12.1059 11.2611 12.1054 11.6827 11.6835C12.1049 11.2611 12.1051 10.5738 11.6832 10.1509Z" fill="#ffffff"/>
                                                        <path d="M8.51892 1.45901C6.57302 -0.486415 3.40746 -0.486415 1.46132 1.45949C-0.486745 3.40491 -0.486985 6.57288 1.45988 8.51902C3.40674 10.4642 6.57302 10.4642 8.51892 8.51878C10.4648 6.57288 10.4646 3.40467 8.51892 1.45901ZM2.37715 7.6027C0.936441 6.162 0.936921 3.8165 2.37763 2.37724C3.81833 0.936771 6.16239 0.936771 7.60261 2.37652C9.04236 3.8165 9.04188 6.15936 7.60261 7.60222C6.16215 9.03837 3.81833 9.04365 2.37715 7.6027Z" fill="#ffffff"/>
                                                        <path d="M3.4035 5.39105C3.29766 4.95617 2.86182 4.35282 2.49511 4.54506C2.20447 4.69338 2.28631 5.37353 2.42335 5.77577C3.03582 7.60479 5.26324 7.99142 5.49484 7.76007C5.7226 7.53255 3.82302 7.11207 3.4035 5.39105Z" fill="#ffffff"/>
                                                    </svg>
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control type="text" name="your_bank" placeholder="Your bank name"/>
                                            <ul className="list-unstyled edited-selected-list">
                                                <li className="">
                                                    <a href="#/"><img src={hsbc_icon} alt="HSBC Icon" className="img-fluid" /> HSBC x-4231</a>
                                                </li>
                                                <li className="">
                                                    <a href="#/"><img src={monzo_icon} alt="MONZO Icon" className="img-fluid" /> HSBC x-4231</a>
                                                </li>
                                                <li className="">
                                                    <a href="#/"><img src={natwest_icon} alt="Natwest Icon" className="img-fluid" /> HSBC x-4231</a>
                                                </li>
                                            </ul>
                                        </InputGroup>
                                    </FormGroup>
                                </Form>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </div>
            <div className={"success-modal__content "+(CheckoutSuccess ? 'd-block' : 'd-none')}>
                <Modal.Header>
                    <div className="modal-logo">
                        <svg width="160" height="16" viewBox="0 0 160 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.119 7.63429C20.5301 7.98476 21.6226 8.49524 22.3965 9.16571C23.1704 9.83619 23.5573 10.6438 23.5573 11.5886C23.5573 12.9905 22.6924 14.08 20.9626 14.8571C19.2328 15.619 16.7291 16 13.4516 16H0V0H12.7687C15.9552 0 18.3565 0.380952 19.9725 1.14286C21.6113 1.90476 22.4306 2.9181 22.4306 4.18286C22.4306 4.92952 22.1461 5.6 21.5771 6.19429C21.0309 6.78857 20.2115 7.26857 19.119 7.63429ZM7.92071 3.77143V6.14857H11.6762C13.4288 6.14857 14.3051 5.75238 14.3051 4.96C14.3051 4.16762 13.4288 3.77143 11.6762 3.77143H7.92071ZM12.7687 12.2286C14.5441 12.2286 15.4317 11.8095 15.4317 10.9714C15.4317 10.1333 14.5441 9.71429 12.7687 9.71429H7.92071V12.2286H12.7687Z" fill="white"/>
                            <path d="M39.2771 13.2114H30.1956L28.591 16H20.3971L30.8443 0H38.765L49.2121 16H40.8817L39.2771 13.2114ZM37.0579 9.32571L34.7363 5.30286L32.4148 9.32571H37.0579Z" fill="white"/>
                            <path d="M70.6412 0V16H64.0179L54.9364 8.73143V16H47.0839V0H53.7073L62.7888 7.26857V0H70.6412Z" fill="white"/>
                            <path d="M80.8395 10.6971L79.0301 12.0457V16H71.1093V0H79.0301V5.80571L86.8825 0H95.6567L86.0972 7.04L96.1347 16H86.8142L80.8395 10.6971Z" fill="white"/>
                            <path d="M105.307 0C107.514 0 109.438 0.24381 111.076 0.731429C112.715 1.21905 113.978 1.92 114.866 2.83429C115.754 3.74857 116.198 4.81524 116.198 6.03429C116.198 7.25333 115.754 8.32 114.866 9.23429C113.978 10.1486 112.715 10.8495 111.076 11.3371C109.438 11.8248 107.514 12.0686 105.307 12.0686H101.858V16H93.8011V0H105.307ZM104.795 7.90857C105.887 7.90857 106.706 7.74857 107.253 7.42857C107.799 7.09333 108.072 6.62857 108.072 6.03429C108.072 5.44 107.799 4.98286 107.253 4.66286C106.706 4.32762 105.887 4.16 104.795 4.16H101.858V7.90857H104.795Z" fill="white"/>
                            <path d="M130.511 13.2114H121.429L119.825 16H111.631L122.078 0H129.999L140.446 16H132.116L130.511 13.2114ZM128.292 9.32571L125.97 5.30286L123.649 9.32571H128.292Z" fill="white"/>
                            <path d="M150.987 10.1943V16H142.929V10.1257L133.916 0H142.417L147.3 5.53143L152.216 0H160L150.987 10.1943Z" fill="white"/>
                        </svg>
                    </div>
                    <span className="closing-checkoutTime">
                        Closing in 0:10
                    </span>
                </Modal.Header>
                <div className="modal-points-label">
                    YOU JUST EARNED 188 POINTS!
                </div>
                <Modal.Body>
                    <div className="success-modal__detail">
                        <h4 className="title">PAYMENT SUCCESSFUL</h4>
                        <ul className="list-unstyled order-detail">
                            <li>
                                <label className="label">Name</label>
                                <span className="value">Urvesh Vasani</span>
                            </li>
                            <li>
                                <label className="label">Ship to</label>
                                <span className="value">Flat 106, The Oxygen - E16 1BQ</span>
                            </li>
                            <li>
                                <label className="label">Phone</label>
                                <span className="value">+44 7539227305</span>
                            </li>
                            <li>
                                <label className="label">Payment</label>
                                <span className="value"><img src={hsbc_icon} alt="HSBC Icon" className="img-fluid" /> HSBC x-4231 - E16 1BQ</span>
                            </li>
                        </ul>
                        <ul className="list-unstyled order-price-detail">
                            <li>
                                <label className="label">SUBTOTAL</label>
                                <span className="value">£120.00</span>
                            </li>
                            <li>
                                <label className="label">SHIPPING</label>
                                <span className="value">£0.00</span>
                            </li>
                            <li>
                                <label className="label">TAX</label>
                                <span className="value">£0.00</span>
                            </li>
                            <li>
                                <label className="label">TOTAL</label>
                                <span className="value">£120.00</span>
                            </li>
                        </ul>

                        <div className="success-modal__action">
                            <a href="#/" className="link link-white" onClick={(e) => CheckoutSuccess ? setCheckoutSuccess(false) : setCheckoutSuccess(true)}>
                                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M-3.4969e-07 7.99997C-3.37156e-07 7.71322 0.0960371 7.4265 0.287709 7.20788L6.32224 0.328227C6.70612 -0.109409 7.3285 -0.109409 7.71221 0.328227C8.09593 0.765686 8.09593 1.4751 7.71221 1.91277L2.37252 7.99997L7.71203 14.0872C8.09574 14.5249 8.09574 15.2342 7.71203 15.6716C7.32831 16.1095 6.70593 16.1095 6.32206 15.6716L0.287522 8.79207C0.0958196 8.57334 -3.6222e-07 8.28662 -3.4969e-07 7.99997Z" fill="white"/>
                                </svg>
                                Redeem points
                            </a>

                            <a href="#/" className="link link-white" onClick={handleClose} >
                                Continue shopping
                                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 8.00003C8 8.28678 7.90396 8.5735 7.71229 8.79212L1.67776 15.6718C1.29388 16.1094 0.671503 16.1094 0.287787 15.6718C-0.095929 15.2343 -0.095929 14.5249 0.287787 14.0872L5.62748 8.00003L0.287974 1.91279C-0.0957423 1.47515 -0.0957423 0.765811 0.287974 0.328387C0.67169 -0.109461 1.29407 -0.109461 1.67794 0.328387L7.71248 7.20793C7.90418 7.42666 8 7.71338 8 8.00003Z" fill="white"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </Modal.Body>
            </div>
        </Modal>
    );
}

ReturningModal.propTypes = {
  show: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};
