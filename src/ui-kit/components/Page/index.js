import React from 'react';

const Page = (props) => {
    const styles = {
    };
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <div className="page" style={styles} {...props} />;
};

export default Page;
