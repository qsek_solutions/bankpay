import React from 'react';

import {
    Navbar, Nav, Container
} from 'react-bootstrap';

import './styles.scss';

export default function NavBar() {
    return (
        
        <header className="main-header">
            <div className="topbar">
                <Container>
                    <p>FREE 3 months worth of transactions if you sign up before 01 May 2021. Terms Apply. <a href="/#">Learn More</a></p>
                </Container>
            </div>
            <Container>
                <Navbar expand="lg" className="p-0">
                    <Navbar.Brand href="#home">
                    <svg width="160" height="16" viewBox="0 0 160 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.119 7.63429C20.5301 7.98476 21.6226 8.49524 22.3965 9.16571C23.1704 9.83619 23.5573 10.6438 23.5573 11.5886C23.5573 12.9905 22.6924 14.08 20.9626 14.8571C19.2328 15.619 16.7291 16 13.4516 16H0V0H12.7687C15.9552 0 18.3565 0.380952 19.9725 1.14286C21.6113 1.90476 22.4306 2.9181 22.4306 4.18286C22.4306 4.92952 22.1461 5.6 21.5771 6.19429C21.0309 6.78857 20.2115 7.26857 19.119 7.63429ZM7.92071 3.77143V6.14857H11.6762C13.4288 6.14857 14.3051 5.75238 14.3051 4.96C14.3051 4.16762 13.4288 3.77143 11.6762 3.77143H7.92071ZM12.7687 12.2286C14.5441 12.2286 15.4317 11.8095 15.4317 10.9714C15.4317 10.1333 14.5441 9.71429 12.7687 9.71429H7.92071V12.2286H12.7687Z" fill="black"/>
                        <path d="M39.2771 13.2114H30.1956L28.591 16H20.3971L30.8443 0H38.765L49.2121 16H40.8817L39.2771 13.2114ZM37.0579 9.32571L34.7363 5.30286L32.4148 9.32571H37.0579Z" fill="black"/>
                        <path d="M70.6412 0V16H64.0179L54.9364 8.73143V16H47.0839V0H53.7073L62.7888 7.26857V0H70.6412Z" fill="black"/>
                        <path d="M80.8395 10.6971L79.0301 12.0457V16H71.1093V0H79.0301V5.80571L86.8825 0H95.6567L86.0972 7.04L96.1347 16H86.8142L80.8395 10.6971Z" fill="black"/>
                        <path d="M105.307 0C107.514 0 109.438 0.24381 111.076 0.731429C112.715 1.21905 113.978 1.92 114.866 2.83429C115.754 3.74857 116.198 4.81524 116.198 6.03429C116.198 7.25333 115.754 8.32 114.866 9.23429C113.978 10.1486 112.715 10.8495 111.076 11.3371C109.438 11.8248 107.514 12.0686 105.307 12.0686H101.858V16H93.8011V0H105.307ZM104.795 7.90857C105.887 7.90857 106.706 7.74857 107.253 7.42857C107.799 7.09333 108.072 6.62857 108.072 6.03429C108.072 5.44 107.799 4.98286 107.253 4.66286C106.706 4.32762 105.887 4.16 104.795 4.16H101.858V7.90857H104.795Z" fill="black"/>
                        <path d="M130.511 13.2114H121.429L119.825 16H111.631L122.078 0H129.999L140.446 16H132.116L130.511 13.2114ZM128.292 9.32571L125.97 5.30286L123.649 9.32571H128.292Z" fill="black"/>
                        <path d="M150.987 10.1943V16H142.929V10.1257L133.916 0H142.417L147.3 5.53143L152.216 0H160L150.987 10.1943Z" fill="black"/>
                    </svg>
                    </Navbar.Brand>
                    
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ml-auto">
                            <Nav.Link href="#home" className="active">Bussiness</Nav.Link>
                            <Nav.Link href="#home">Personal</Nav.Link>
                            <Nav.Link href="#link">Get started</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                    <a href="#link" className="login-link">Log In 
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13.6569 10.3431C12.7855 9.47181 11.7484 8.82678 10.6168 8.43631C11.8288 7.60159 12.625 6.20463 12.625 4.625C12.625 2.07478 10.5502 0 8 0C5.44978 0 3.375 2.07478 3.375 4.625C3.375 6.20463 4.17122 7.60159 5.38319 8.43631C4.25163 8.82678 3.2145 9.47181 2.34316 10.3431C0.832156 11.8542 0 13.8631 0 16H1.25C1.25 12.278 4.27803 9.25 8 9.25C11.722 9.25 14.75 12.278 14.75 16H16C16 13.8631 15.1678 11.8542 13.6569 10.3431ZM8 8C6.13903 8 4.625 6.486 4.625 4.625C4.625 2.764 6.13903 1.25 8 1.25C9.86097 1.25 11.375 2.764 11.375 4.625C11.375 6.486 9.86097 8 8 8Z" fill="black"/>
                        </svg>
                    </a>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                </Navbar>
            </Container>
        </header>
    );
}
