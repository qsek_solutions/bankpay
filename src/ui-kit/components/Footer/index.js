import React from 'react';

import {
    Container, Row, Col, Dropdown
} from 'react-bootstrap';

import footerLogo from 'assets/images/bankpay-logo.png';
import uk_flag from 'assets/images/uk_flag.png';
import ind_flag from 'assets/images/india-flag-icon.png';
import us_flag from 'assets/images/us-flag.png';

import './styles.scss';

export default function Footer() {
    return (
        <footer className="main-footer landing-page-footer">
            <Container>
                <Row md={3}>
                    <Col lg={4}>
                        <div className="footer-card">
                            <div className="footer-logo">
                                <img src={footerLogo} className="img-fluid" alt="Logo Footer" />
                            </div>
                            <p className="footer-address-text">106, The Oxygen<br />
                            18 Western Gateway<br />
                            London E16 1BQ</p>
                            <Dropdown className="flag-dropdown">
                                <Dropdown.Toggle className="" id="dropdown-basic">
                                    <img src={uk_flag} className="img-fluid" alt="UK Flag" /> United Kingdom
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1"><img src={uk_flag} className="img-fluid" alt="UK Flag" /> United Kingdom</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2"><img src={ind_flag} className="img-fluid" alt="UK Flag" /> India</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3"><img src={us_flag} className="img-fluid" alt="UK Flag" /> United State</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </Col>
                    <Col lg={2}>
                        <div className="footer-card">
                            <label>Consumers</label>
                            <ul className="list-unstyled footer-nav">
                                <li className="">
                                    <a href="/#">Join Waitlist</a>
                                </li>
                                <li className="">
                                    <a href="/#"> Support </a>
                                </li>
                                <li className="">
                                    <a href="/#">FAQ</a>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col lg={2}>
                        <div className="footer-card">
                            <label>Consumers</label>
                            <ul className="list-unstyled footer-nav">
                                <li className="">
                                    <a href="/#">Join Waitlist</a>
                                </li>
                                <li className="">
                                    <a href="/#">Support</a>
                                </li>
                                <li className="">
                                    <a href="/#">FAQ</a>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col lg={2}>
                        <div className="footer-card">
                            <label>Merchants</label>
                            <ul className="list-unstyled footer-nav">
                                <li className="">
                                    <a href="/#">Apply</a>
                                </li>
                                <li className="">
                                    <a href="/#">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col lg={2}>
                        <div className="footer-card">
                            <label className="service-status-title">Service Status</label>
                        </div>
                    </Col>
                </Row>
                <div className="landing-footer-title">
                    <h3 className="footer-title-text">NO CREDIT CARDS.<br />NO INTERCHANGE.<br />NO CC DEBT.<br />NO CARD FRAUD.<br />NO LOST CARDS.<br />NO INVALID CVV.</h3>
                </div>
            </Container>
        </footer>
    );
}
