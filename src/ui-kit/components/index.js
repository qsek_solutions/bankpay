export { default as Page } from './Page';
export { default as NavBar } from './NavBar';
export { default as Footer } from './Footer';
export { default as SideBar } from './SideBar';