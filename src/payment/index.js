import React from 'react';
// import $ from "jquery";
import { NavLink } from 'react-router-dom';


import './styles.scss';

import { Page, SideBar } from '@ui-kit';
import { Nav, Table } from 'react-bootstrap';

// import { Container, Navbar,Nav, Form, Button  } from 'react-bootstrap';


export default class Payment extends React.Component {

    componentDidMount() {
    }

    render() {
        return (

            <Page className="BP_Payment BP_SideBar-page">
                <SideBar />
                <div className="BP_SideBar-page__content">
                    <div className="BP_SideBar-page__header">
                        <a href="#/" className="sideber-toggler d-inline-flex d-lg-none">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                <path d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </a>
                        <Nav>
                            <Nav.Item>
                                <NavLink to="/account" className="nav-link" activeClassName="active">Account</NavLink>
                            </Nav.Item>
                            <Nav.Item>
                                <NavLink to="login" className="nav-link" activeClassName="active">Log Out</NavLink>
                            </Nav.Item>
                        </Nav>
                    </div>

                    <div className="BP_Payment__content-wrapper">
                        <h2 className="BP_page-title">Payments</h2>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th>AMOUNT</th>
                                    <th></th>
                                    <th>TXN ID</th>
                                    <th>ORDER ID</th>
                                    <th>CUSTOMER</th>
                                    <th>DATE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>£9.99</td>
                                    <td><span className="badge badge-success">Successful</span></td>
                                    <td>t_17h9hdkg78</td>
                                    <td>o_17h9hdkg78</td>
                                    <td>urveshvasani@protonmail.ch</td>
                                    <td>18 Mar, 11:55</td>
                                </tr>
                                <tr>
                                    <td>£9.99</td>
                                    <td><span className="badge badge-danger">Failed</span></td>
                                    <td>t_564uihdc7n</td>
                                    <td>t_564uihdc7n</td>
                                    <td>urveshvasani@protonmail.ch</td>
                                    <td>18 Mar, 11:55</td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>
            </Page>
        );
    }
}
